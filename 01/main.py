
from lib import main

def get_fuel(mass: int) -> int:
    return mass//3 - 2

def get_fuel_recursive(mass: int) -> int:
    output = 0
    while get_fuel(mass) > 0:
        mass = get_fuel(mass)
        output += mass
    return output

def test_part1():
    assert get_fuel(12) == 2
    assert get_fuel(14) == 2
    assert get_fuel(1969) == 654
    assert get_fuel(100756) == 33583

def test_part2():
    assert get_fuel_recursive(12) == 2
    assert get_fuel_recursive(1969) == 966
    assert get_fuel_recursive(100756) == 50346

def part1(file) -> int:
    with open(file) as f:
        data = f.readlines()

    data = map(int, data)

    total = 0
    for value in data:
        total += get_fuel(value)

    return total

def part2(file) -> int:
    with open(file) as f:
        data = f.readlines()

    data = map(int, data)

    total = 0
    for value in data:
        total += get_fuel_recursive(value)

    return total

if __name__ == "__main__":
    main(part1, part2)
