
import copy

from lib import main
from typing import List

def run_program(intcode: List[int]) -> List[int]:
    ptr = 0
    intcode = copy.deepcopy(intcode)
    while True:
        opcode = intcode[ptr]
        if opcode == 1:
            firstaddr = intcode[ptr+1]
            secondaddr = intcode[ptr+2]
            dstaddr = intcode[ptr+3]
            intcode[dstaddr] = intcode[firstaddr] + intcode[secondaddr]
            ptr += 4
        elif opcode == 2:
            firstaddr = intcode[ptr+1]
            secondaddr = intcode[ptr+2]
            dstaddr = intcode[ptr+3]
            intcode[dstaddr] = intcode[firstaddr] * intcode[secondaddr]
            ptr += 4
        elif opcode == 99:
            return intcode
        else:
            raise ValueError("Invalid opcode: {}".format(opcode))


def test_part1():
    assert run_program([1,0,0,0,99]) == [2,0,0,0,99]
    assert run_program([2,3,0,3,99]) == [2,3,0,6,99]
    assert run_program([2,4,4,5,99,0]) == [2,4,4,5,99,9801]
    assert run_program([1,1,1,4,99,5,6,0,99]) == [30,1,1,4,2,5,6,0,99]


def test_part2():
    pass

def part1(file):
    with open(file) as f:
        data = f.read()
    
    intcode = list(map(int, data.split(',')))
    intcode[1] = 12
    intcode[2] = 2
    output = run_program(intcode)
    return output[0]

def part2(file):
    for noun in range(100):
        for verb in range(100):
            with open(file) as f:
                data = f.read()
            intcode = list(map(int, data.split(',')))
            intcode[1] = noun
            intcode[2] = verb
            output = run_program(intcode)
            if output[0] == 19690720:
                return 100*noun+verb
    return False

if __name__ == "__main__":
    main(part1, part2)