
from typing import Any, Callable

def main(part1: Callable[[str], Any], part2: Callable[[str], Any]) -> None:
    try:
        import pytest
    except ImportError:
        print("pytest not installed, won't test")
    else:
        print("Tests: ")
        pytest.main(['-q', 'main.py'])
    print("Part 1 output: {}".format(part1('input')))
    print("Part 2 output: {}".format(part2('input')))