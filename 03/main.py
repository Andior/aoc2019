
from lib import main
from math import inf
from typing import List, Tuple

def get_points(instructions: List[str]) -> List[Tuple[int,int]]:
    points = []
    current = (0,0)
    for instruction in instructions:
        direction, value = instruction[0], int(instruction[1:])
        if direction == 'U':
            delta = (0, 1)
        elif direction == 'D':
            delta = (0, -1)
        elif direction == 'L':
            delta = (-1, 0)
        elif direction == 'R':
            delta = (1, 0)
        else:
            raise ValueError("Unknown direction")
        for i in range(1,value+1):
            points.append((current[0]+delta[0]*i,current[1]+delta[1]*i))
        current = (current[0]+delta[0]*value, current[1]+delta[1]*value)
    return points

def get_nearest_intersection(wire1, wire2):
    points1 = get_points(wire1)
    points2 = get_points(wire2)
    return min(map(lambda x: abs(x[0])+abs(x[1]), set(points1) & set(points2)))

def get_closest_intersection(wire1, wire2):
    points1 = get_points(wire1)
    points2 = get_points(wire2)
    return min(map(lambda x: points1.index(x) + points2.index(x) + 2, set(points1) & set(points2)))

def test_getpoints():
    assert get_points(['U2']) == [(0,1),(0,2)]
    assert get_points(['U2','R3']) == [(0,1),(0,2),(1,2),(2,2),(3,2)]

def test_nearest():
    assert get_nearest_intersection(['R8','U5','L5','D3'], ['U7', 'R6','D4','L4']) == 6

def test_part1():
    assert get_nearest_intersection(['R75', 'D30', 'R83', 'U83', 'L12', 'D49', 'R71', 'U7', 'L72'], ['U62', 'R66', 'U55', 'R34', 'D71', 'R55', 'D58', 'R83']) == 159
    assert get_nearest_intersection(['R98', 'U47', 'R26', 'D63', 'R33', 'U87', 'L62', 'D20', 'R33', 'U53', 'R51'], ['U98', 'R91', 'D20', 'R16', 'D67', 'R40', 'U7', 'R15', 'U6', 'R7']) == 135

def test_part2():
    assert get_closest_intersection(['R75', 'D30', 'R83', 'U83', 'L12', 'D49', 'R71', 'U7', 'L72'], ['U62', 'R66', 'U55', 'R34', 'D71', 'R55', 'D58', 'R83']) == 610
    assert get_closest_intersection(['R98', 'U47', 'R26', 'D63', 'R33', 'U87', 'L62', 'D20', 'R33', 'U53', 'R51'], ['U98', 'R91', 'D20', 'R16', 'D67', 'R40', 'U7', 'R15', 'U6', 'R7']) == 410

def part1(file):
    with open('input') as f:
        lines = f.readlines()
    wire1 = lines[0].split(',')
    wire2 = lines[1].split(',')
    return get_nearest_intersection(wire1, wire2)

def part2(file):
    with open('input') as f:
        lines = f.readlines()
    wire1 = lines[0].split(',')
    wire2 = lines[1].split(',')
    return get_closest_intersection(wire1, wire2)

if __name__ == "__main__":
    main(part1, part2)